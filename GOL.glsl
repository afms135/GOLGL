#version 150 core
in vec2 forfrag;
out vec4 outcol;
uniform sampler2D state;
uniform int size;
const vec2 rules[9] = vec2[](
	vec2(0,0),
	vec2(0,0),
	vec2(1,0),	//2 neighbours = lives
	vec2(0,1),	//3 neighbours = born
	vec2(0,0),
	vec2(0,0),
	vec2(0,0),
	vec2(0,0),
	vec2(0,0)
);

int get(vec2 offset)
{
	vec2 coord = forfrag + offset / size;
	vec4 texel = texture(state, coord);
	return int(texel.r);
}

void main()
{
	int sum =
		get(vec2(-1, -1)) +
		get(vec2(-1,  0)) +
		get(vec2(-1,  1)) +
		get(vec2( 0, -1)) +
		get(vec2( 0,  1)) +
		get(vec2( 1, -1)) +
		get(vec2( 1,  0)) +
		get(vec2( 1,  1));

	vec2 rule = rules[sum];
	float col = get(vec2(0,0)) * rule.x + rule.y;
	outcol = vec4(col, col, col, 1.0);
}
