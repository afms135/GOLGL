#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#ifdef WIN32
#include <SDL.h>
#else
#include <SDL2/SDL.h>
#endif
#include "gl_core_3_2.h"

#define BOARDSIZE 768	//Must be a power of 2

char vertex_shader[] =
{
	"#version 150 core\n"
	"in vec2 vertex;"
	"in vec2 vertexuv;"
	"out vec2 uv;"

	"void main()"
	"{"
	"	gl_Position = vec4(vertex, 0.0, 1.0);"
	"	uv = vertexuv;"
	"}"
};

char fragment_shader[] =
{
	"#version 150 core\n"
	"in vec2 uv;"
	"out vec4 outcol;"
	"uniform sampler2D tex;"

	"void main()"
	"{"
	"	outcol = texture(tex, uv);"
	"}"
};

char GOL_shader[] =
{
	"#version 150 core\n"
	"in vec2 uv;"
	"out vec4 outcol;"
	"uniform sampler2D state;"
	"uniform int size;"
	"const vec2 rules[9] = vec2[]("
	"	vec2(0, 0),"
	"	vec2(0, 0),"
	"	vec2(1, 0),"
	"	vec2(0, 1),"
	"	vec2(0, 0),"
	"	vec2(0, 0),"
	"	vec2(0, 0),"
	"	vec2(0, 0),"
	"	vec2(0, 0)"
	"	);"

	"int get(vec2 offset)"
	"{"
	"	vec2 coord = uv + offset / size;"
	"	vec4 texel = texture(state, coord);"
	"	return int(texel.r);"
	"}"

	"void main()"
	"{"
	"	int sum ="
	"		get(vec2(-1, -1)) +"
	"		get(vec2(-1, 0)) +"
	"		get(vec2(-1, 1)) +"
	"		get(vec2(0, -1)) +"
	"		get(vec2(0, 1)) +"
	"		get(vec2(1, -1)) +"
	"		get(vec2(1, 0)) +"
	"		get(vec2(1, 1));"

	"	vec2 rule = rules[sum];"
	"	float col = get(vec2(0, 0)) * rule.x + rule.y;"
	"	outcol = vec4(col, col, col, 1.0);"
	"}"
};

int load_shader(const GLchar *vertex, const GLchar *fragment, GLuint *program);
void seed_texture(GLuint tex);

int main(void)
{
	//
	// Setup OpenGL Context
	//
	SDL_Init(SDL_INIT_VIDEO);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	SDL_Window *win = SDL_CreateWindow("LifeGL", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, BOARDSIZE, BOARDSIZE, SDL_WINDOW_OPENGL);
	SDL_GLContext cxt = SDL_GL_CreateContext(win);
	ogl_LoadFunctions();
	glViewport(0, 0, BOARDSIZE, BOARDSIZE);
	srand((unsigned int)time(NULL));

	//
	// Vertex data (X,Y,U,V)
	//
	const GLfloat vertex[] =
	{
		-1.0, -1.0,		0.0, 0.0,
		-1.0,  1.0,		0.0, 1.0,
		 1.0, -1.0,		1.0, 0.0,
		 1.0,  1.0,	 	1.0, 1.0,
	};

	//
	// Init VAO and VBO
	//
	GLuint VAO;
	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);

	GLuint VBO;
	glGenBuffers(1, &VBO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertex), vertex, GL_STATIC_DRAW);

	//
	// Init rendering shader
	//
	GLuint shader_show;
	if (load_shader(vertex_shader, fragment_shader, &shader_show))
		goto E1;
	
	GLint att_renderver = glGetAttribLocation(shader_show, "vertex");
	glEnableVertexAttribArray(att_renderver);
	glVertexAttribPointer(att_renderver, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), 0);

	GLint att_renderuv = glGetAttribLocation(shader_show, "vertexuv");
	glEnableVertexAttribArray(att_renderuv);
	glVertexAttribPointer(att_renderuv, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (void*)(2 * sizeof(GLfloat)));

	//
	// Init simulation shader
	//
	GLuint shader_GOL;
	if (load_shader(vertex_shader, GOL_shader, &shader_GOL))
		goto E2;
	
	GLint att_GOLver = glGetAttribLocation(shader_GOL, "vertex");
	glEnableVertexAttribArray(att_GOLver);
	glVertexAttribPointer(att_GOLver, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), 0);
	
	GLint att_GOLuv = glGetAttribLocation(shader_GOL, "vertexuv");
	glEnableVertexAttribArray(att_GOLuv);
	glVertexAttribPointer(att_GOLuv, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (void*)(2 * sizeof(GLfloat)));
	
	GLint uni_size = glGetUniformLocation(shader_GOL, "size");

	//
	// Init FBO
	//
	GLuint fbuf;
	glGenFramebuffers(1, &fbuf);

	//
	// Init front and back textures
	//
	GLuint tex_front, tex_back;
	glGenTextures(1, &tex_front);
	glBindTexture(GL_TEXTURE_2D, tex_front);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, BOARDSIZE, BOARDSIZE, 0, GL_RGB, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glGenTextures(1, &tex_back);
	glBindTexture(GL_TEXTURE_2D, tex_back);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, BOARDSIZE, BOARDSIZE, 0, GL_RGB, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	//
	// Seed back texture
	//
	seed_texture(tex_back);

	//
	// Start simulation
	//
	GLuint renderto = tex_front;
	GLuint oldstate = tex_back;
	int run = 1;
	int textw = 0;
	while (run)
	{
		Uint64 start = SDL_GetPerformanceCounter();
		SDL_Event ev;
		while (SDL_PollEvent(&ev))
		{
			if (ev.type == SDL_QUIT)
				run = 0;
			else if (ev.type == SDL_KEYDOWN)
			{
				if (ev.key.keysym.scancode == SDL_SCANCODE_RETURN)
				{
					seed_texture(tex_back);
					renderto = tex_front;
					oldstate = tex_back;
				}
			}
		}

		//Perform simulation
		glBindFramebuffer(GL_FRAMEBUFFER, fbuf);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, renderto, 0);
		glBindTexture(GL_TEXTURE_2D, oldstate);
		glUseProgram(shader_GOL);
		glUniform1i(uni_size, BOARDSIZE);
		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, 0, 0);

		//Render on screen
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glBindTexture(GL_TEXTURE_2D, renderto);
		glUseProgram(shader_show);
		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

		//Flip buffers
		GLuint temp = renderto;
		renderto = oldstate;
		oldstate = temp;
		SDL_GL_SwapWindow(win);

		//Benchmark
		Uint64 delta = SDL_GetPerformanceCounter() - start;
		double diff = (double)(delta) / (double)SDL_GetPerformanceFrequency();
		while(textw--)
			printf("\b \b");
		textw = printf("%.2fms | %.2fHz", diff * 1000, 1 / diff);
		fflush(stdout);
	}
	printf("\n");

	//
	// Cleanup
	//
	//FBO
	glBindFramebuffer(GL_FRAMEBUFFER, fbuf);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, 0, 0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glDeleteFramebuffers(1, &fbuf);
	//Textures
	glBindTexture(GL_TEXTURE_2D, 0);
	glDeleteTextures(1, &tex_front);
	glDeleteTextures(1, &tex_back);
	//Shaders
	glUseProgram(0);
	glDeleteProgram(shader_GOL);
E2:	glDeleteProgram(shader_show);
	//VBO
E1:	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glDeleteBuffers(1, &VBO);
	//VAO
	glBindVertexArray(0);
	glDeleteVertexArrays(1, &VAO);
	//SDL
	SDL_GL_DeleteContext(cxt);
	SDL_DestroyWindow(win);

	return 0;
}

void seed_texture(GLuint tex)
{
	GLbyte *seed = malloc(BOARDSIZE * BOARDSIZE);
	for (int i = 0; i < (BOARDSIZE * BOARDSIZE); i++)
		seed[i] = rand();
	glBindTexture(GL_TEXTURE_2D, tex);
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, BOARDSIZE, BOARDSIZE, GL_RGB, GL_UNSIGNED_BYTE_3_3_2, seed);
	free(seed);
}

int load_shader(const GLchar *vertex, const GLchar *fragment, GLuint *program)
{
	GLint success;
	GLint loglen;
	char *buf;
	
	//Vertex Shader
	GLint shader_vertex = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(shader_vertex, 1, &vertex, NULL);
	glCompileShader(shader_vertex);
	glGetShaderiv(shader_vertex, GL_COMPILE_STATUS, &success);
	glGetShaderiv(shader_vertex, GL_INFO_LOG_LENGTH, &loglen);
	if (loglen > 0)
	{
		buf = malloc(loglen);
		if (buf != NULL)
		{
			glGetShaderInfoLog(shader_vertex, loglen, NULL, buf);
			puts(buf);
			free(buf);
		}
	}
	if (!success)
		return -1;

	//Fragment Shader
	GLint shader_fragment = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(shader_fragment, 1, &fragment, NULL);
	glCompileShader(shader_fragment);
	glGetShaderiv(shader_fragment, GL_COMPILE_STATUS, &success);
	glGetShaderiv(shader_fragment, GL_INFO_LOG_LENGTH, &loglen);
	if (loglen > 0)
	{
		char *buf = malloc(loglen);
		if (buf != NULL)
		{
			glGetShaderInfoLog(shader_fragment, loglen, NULL, buf);
			puts(buf);
			free(buf);
		}
	}
	if (!success)
	{
		glDeleteShader(shader_vertex);
		return -1;
	}
	
	//Link Program
	*program = glCreateProgram();
	glAttachShader(*program, shader_vertex);
	glAttachShader(*program, shader_fragment);
	glLinkProgram(*program);
	glGetProgramiv(*program, GL_LINK_STATUS, &success);
	glGetProgramiv(*program, GL_INFO_LOG_LENGTH, &loglen);
	if (loglen > 0)
	{
		char *buf = malloc(loglen);
		if (buf != NULL)
		{
			glGetProgramInfoLog(*program, loglen, NULL, buf);
			puts(buf);
			free(buf);
		}
	}
	if (!success)
	{
		glDeleteShader(shader_vertex);
		glDeleteShader(shader_fragment);
		return -1;
	}

	//Finish
	glDeleteShader(shader_vertex);
	glDeleteShader(shader_fragment);
	return 0;
}