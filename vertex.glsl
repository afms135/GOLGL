#version 150 core
in vec2 position;
in vec2 tex;
out vec2 forfrag;

void main()
{
    gl_Position = vec4(position, 0.0, 1.0);
	forfrag = tex;
}