#version 150 core
in vec2 forfrag;
out vec4 outcol;
uniform sampler2D tex;

void main()
{
	outcol = texture(tex,forfrag);
}